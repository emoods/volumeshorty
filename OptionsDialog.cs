﻿using System;
using System.Windows.Forms;
using VolumeShorty.Properties;

namespace VolumeShorty
{
	public partial class OptionsDialog : Form
	{
		public OptionsDialog()
		{
			InitializeComponent();
		}

		private KeyEventArgs VolUp;
		private KeyEventArgs VolDown;
		private KeyEventArgs Mute;

		private bool OnHotKeyDetect(TextBox tb, KeyEventArgs e)
		{
			if (tb == null) return false;

			tb.Text = "";

			if (e.Control)
				tb.Text += "Ctrl + ";

			if (e.Alt)
				tb.Text += "Alt + ";

			if (e.Shift)
				tb.Text += "Shift + ";

			e.SuppressKeyPress = true;

			if (!e.KeyCode.HasFlag(Keys.ControlKey) && !e.KeyCode.HasFlag(Keys.Alt) && !e.KeyCode.HasFlag(Keys.ShiftKey))
			{
				tb.Text += e.KeyCode;
				return (e.Modifiers & (Keys.Control | Keys.Alt | Keys.Shift)) != 0;
			}
			return false;
		}


		private void OnVolUpKeyDown(object sender, KeyEventArgs e)
		{

			var tb = sender as TextBox;

			if (tb != null && OnHotKeyDetect(tb, e))
			{
				VolUp = e;
			}

		}

		private void OnVolDownKeyDown(object sender, KeyEventArgs e)
		{
			var tb = sender as TextBox;

			if (tb != null && OnHotKeyDetect(tb, e))
			{
				VolDown = e;
			}
		}

		private void OnMuteKeyDown(object sender, KeyEventArgs e)
		{
			var tb = sender as TextBox;

			if (tb != null && OnHotKeyDetect(tb, e))
			{
				Mute = e;
			}
		}

		[Serializable]
		public class KeyInfo
		{
			public int ModKeyValue;
			public int KeyValue;

			public KeyInfo(KeyEventArgs e)
			{
				ModKeyValue = (int)e.Modifiers;
				KeyValue = e.KeyValue;
			}
		}

		private void OnSave(object sender, EventArgs e)
		{
			Settings.Default.VolUpKey = (int)VolUp.KeyValue;
			Settings.Default.VolUpModKey = (int)VolUp.Modifiers;
			Settings.Default.VolDownKey = (int)VolDown.KeyValue;
			Settings.Default.VolDownModKey = (int)VolDown.Modifiers;
			Settings.Default.MuteKey = (int)Mute.KeyValue;
			Settings.Default.MuteModKey = (int)Mute.Modifiers;
			Settings.Default.Save();
			this.Close();
		}

		private void OnClose(object sender, EventArgs e)
		{
			this.Close();
		}

	}
}
