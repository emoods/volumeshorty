﻿using System;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace VolumeShorty
{
	public class Hotkey : IMessageFilter
	{
		#region Interop

		private const uint WM_HOTKEY = 0x312;

		private const uint MOD_ALT = 0x1;
		private const uint MOD_CONTROL = 0x2;
		private const uint MOD_SHIFT = 0x4;
		private const uint MOD_WIN = 0x8;

		private const uint ERROR_HOTKEY_ALREADY_REGISTERED = 1409;

		[DllImport("user32.dll", SetLastError = true)]
		private static extern int RegisterHotKey(IntPtr hWnd, int id, uint fsModifiers, Keys vk);

		[DllImport("user32.dll", SetLastError = true)]
		private static extern int UnregisterHotKey(IntPtr hWnd, int id);

		#endregion

		private const int maximumID = 0xBFFF;
		private static int currentID;

		private bool alt;
		private bool control;

		[XmlIgnore] private int id;
		private Keys keyCode;
		[XmlIgnore] private bool registered;
		private bool shift;
		[XmlIgnore] private Control windowControl;
		private bool windows;

		public Hotkey()
			: this(Keys.None, 0)
		{
			// No work done here!
		}

		public Hotkey(Keys keyCode, int modifiers)
		{
			// Assign properties
			KeyCode = keyCode;
			Shift = (modifiers & (int) Keys.ShiftKey) != 0;
			Control = (modifiers & (int) Keys.Control) != 0;
			Alt = (modifiers & (int) Keys.Alt) != 0;
			Windows = (modifiers & (int) Keys.LWin & (int) Keys.RWin) != 0;

			// Register us as a message filter
			Application.AddMessageFilter(this);
		}

		public bool Empty
		{
			get { return keyCode == Keys.None; }
		}

		public bool Registered
		{
			get { return registered; }
		}

		public Keys KeyCode
		{
			get { return keyCode; }
			set
			{
				// Save and reregister
				keyCode = value;
				Reregister();
			}
		}

		public bool Shift
		{
			get { return shift; }
			set
			{
				// Save and reregister
				shift = value;
				Reregister();
			}
		}

		public bool Control
		{
			get { return control; }
			set
			{
				// Save and reregister
				control = value;
				Reregister();
			}
		}

		public bool Alt
		{
			get { return alt; }
			set
			{
				// Save and reregister
				alt = value;
				Reregister();
			}
		}

		public bool Windows
		{
			get { return windows; }
			set
			{
				// Save and reregister
				windows = value;
				Reregister();
			}
		}

		public bool PreFilterMessage(ref Message message)
		{
			// Only process WM_HOTKEY messages
			if (message.Msg != WM_HOTKEY)
			{
				return false;
			}

			// Check that the ID is our key and we are registerd
			if (registered && (message.WParam.ToInt32() == id))
			{
				// Fire the event and pass on the event if our handlers didn't handle it
				return OnPressed();
			}
			else
			{
				return false;
			}
		}

		public event HandledEventHandler Pressed;

		~Hotkey()
		{
			// Unregister the hotkey if necessary
			if (Registered)
			{
				Unregister();
			}
		}


		public bool GetCanRegister(Control windowControl)
		{
			// Handle any exceptions: they mean "no, you can't register" :)
			try
			{
				// Attempt to register
				if (!Register(windowControl))
				{
					return false;
				}

				// Unregister and say we managed it
				Unregister();
				return true;
			}
			catch (Win32Exception)
			{
				return false;
			}
			catch (NotSupportedException)
			{
				return false;
			}
		}

		public bool Register(Control windowControl)
		{
			// Check that we have not registered
			if (registered)
			{
				throw new NotSupportedException("You cannot register a hotkey that is already registered");
			}

			// We can't register an empty hotkey
			if (Empty)
			{
				return false;
			}

			// Get an ID for the hotkey and increase current ID
			id = currentID;
			currentID = currentID + 1%maximumID;

			// Translate modifier keys into unmanaged version
			uint modifiers = (Alt ? MOD_ALT : 0) | (Control ? MOD_CONTROL : 0) |
							(Shift ? MOD_SHIFT : 0) | (Windows ? MOD_WIN : 0);

			// Register the hotkey
			if (RegisterHotKey(windowControl.Handle, id, modifiers, keyCode) == 0)
			{
				// Is the error that the hotkey is registered?
				if (Marshal.GetLastWin32Error() == ERROR_HOTKEY_ALREADY_REGISTERED)
				{
					return false;
				}
				else
				{
					throw new Win32Exception();
				}
			}

			// Save the control reference and register state
			registered = true;
			this.windowControl = windowControl;

			// We successfully registered
			return true;
		}

		public void Unregister()
		{
			// Check that we have registered
			if (!registered)
			{
				return;
			}

			// It's possible that the control itself has died: in that case, no need to unregister!
			if (!windowControl.IsDisposed)
			{
				// Clean up after ourselves
				if (UnregisterHotKey(windowControl.Handle, id) == 0)
				{
					throw new Win32Exception();
				}
			}

			// Clear the control reference and register state
			registered = false;
			windowControl = null;
		}

		private void Reregister()
		{
			// Only do something if the key is already registered
			if (!registered)
			{
				return;
			}

			// Save control reference
			Control windowControl = this.windowControl;

			// Unregister and then reregister again
			Unregister();
			Register(windowControl);
		}

		private bool OnPressed()
		{
			// Fire the event if we can
			var handledEventArgs = new HandledEventArgs(false);
			if (Pressed != null)
			{
				Pressed(this, handledEventArgs);
			}

			// Return whether we handled the event or not
			return handledEventArgs.Handled;
		}

		public override string ToString()
		{
			// We can be empty
			if (Empty)
			{
				return "(none)";
			}

			// Build key name
			string keyName = Enum.GetName(typeof (Keys), keyCode);
			;
			switch (keyCode)
			{
				case Keys.D0:
				case Keys.D1:
				case Keys.D2:
				case Keys.D3:
				case Keys.D4:
				case Keys.D5:
				case Keys.D6:
				case Keys.D7:
				case Keys.D8:
				case Keys.D9:
					// Strip the first character
					keyName = keyName.Substring(1);
					break;
				default:
					// Leave everything alone
					break;
			}

			// Build modifiers
			string modifiers = "";
			if (shift)
			{
				modifiers += "Shift+";
			}
			if (control)
			{
				modifiers += "Control+";
			}
			if (alt)
			{
				modifiers += "Alt+";
			}
			if (windows)
			{
				modifiers += "Windows+";
			}

			// Return result
			return modifiers + keyName;
		}
	}
}