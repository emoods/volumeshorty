﻿using System;
using System.Runtime.InteropServices;

namespace VolumeShorty
{
	internal static class SoundService
	{
		private const int APPCOMMAND_VOLUME_MUTE = 0x80000;
		private const int APPCOMMAND_VOLUME_UP = 0xa0000;
		private const int APPCOMMAND_VOLUME_DOWN = 0x90000;
		private const int WM_APPCOMMAND = 0x319;

		[DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = false)]
		private static extern IntPtr SendMessage(IntPtr hWnd, uint Msg, IntPtr wParam, IntPtr lParam);

		public static void VolumeDown(IntPtr handler)
		{
			SendMessage(handler, WM_APPCOMMAND, handler, new IntPtr(APPCOMMAND_VOLUME_DOWN));
		}

		public static void VolumeUp(IntPtr handler)
		{
			SendMessage(handler, WM_APPCOMMAND, handler, new IntPtr(APPCOMMAND_VOLUME_UP));
		}

		public static void Mute(IntPtr handler)
		{
			SendMessage(handler, WM_APPCOMMAND, handler, new IntPtr(APPCOMMAND_VOLUME_MUTE));
		}
	}
}