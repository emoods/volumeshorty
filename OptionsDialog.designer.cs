﻿namespace VolumeShorty
{
	partial class OptionsDialog
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.button1 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.VolumeUpHotKey = new System.Windows.Forms.TextBox();
			this.VolumeDownHotKey = new System.Windows.Forms.TextBox();
			this.MuteHotKey = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(75, 93);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(75, 23);
			this.button1.TabIndex = 0;
			this.button1.Text = "Save";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.OnSave);

			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(175, 93);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(75, 23);
			this.button2.TabIndex = 1;
			this.button2.Text = "Cancel";
			this.button2.UseVisualStyleBackColor = true;
			this.button2.Click += new System.EventHandler(this.OnClose);
			// 
			// VolumeUpHotKey
			// 
			this.VolumeUpHotKey.Location = new System.Drawing.Point(15, 11);
			this.VolumeUpHotKey.Name = "VolumeUpHotKey";
			this.VolumeUpHotKey.Size = new System.Drawing.Size(100, 20);
			this.VolumeUpHotKey.TabIndex = 2;
			this.VolumeUpHotKey.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OnVolUpKeyDown);
			// 
			// VolumeDownHotKey
			// 
			this.VolumeDownHotKey.Location = new System.Drawing.Point(15, 37);
			this.VolumeDownHotKey.Name = "VolumeDownHotKey";
			this.VolumeDownHotKey.Size = new System.Drawing.Size(100, 20);
			this.VolumeDownHotKey.TabIndex = 3;
			this.VolumeDownHotKey.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OnVolDownKeyDown);
			// 
			// MuteHotKey
			// 
			this.MuteHotKey.Location = new System.Drawing.Point(15, 63);
			this.MuteHotKey.Name = "MuteHotKey";
			this.MuteHotKey.Size = new System.Drawing.Size(100, 20);
			this.MuteHotKey.TabIndex = 4;
			this.MuteHotKey.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OnMuteKeyDown);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(138, 14);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(93, 13);
			this.label1.TabIndex = 5;
			this.label1.Text = "VolumeUp Hotkey";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(138, 40);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(108, 13);
			this.label2.TabIndex = 6;
			this.label2.Text = "VolumeDown HotKey";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(138, 66);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(68, 13);
			this.label3.TabIndex = 7;
			this.label3.Text = "Mute Hotkey";
			// 
			// OptionsDialog
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(262, 128);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.MuteHotKey);
			this.Controls.Add(this.VolumeDownHotKey);
			this.Controls.Add(this.VolumeUpHotKey);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.button1);
			this.Name = "OptionsDialog";
			this.Text = "OptionsDialog";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.TextBox VolumeUpHotKey;
		private System.Windows.Forms.TextBox VolumeDownHotKey;
		private System.Windows.Forms.TextBox MuteHotKey;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
	}
}