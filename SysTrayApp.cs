﻿using System;
using System.Windows.Forms;
using VolumeShorty.Properties;

namespace VolumeShorty
{
	public partial class SysTrayApp : Form
	{
		private Hotkey _volUp;
		private Hotkey _volDown;
		private Hotkey _mute;

		public SysTrayApp()
		{
			InitializeComponent();
		}

		private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
		{

		}

		private void exitToolStripMenuItem_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		protected override void OnLoad(EventArgs e)
		{
			Visible = false; // Hide form window.
			ShowInTaskbar = false; // Remove from taskbar.

			base.OnLoad(e);
		}

		private void Form1_Load(object sender, EventArgs e)
		{
			LoadSettings();
		}
	
		private void LoadSettings()
		{
			_volUp = new Hotkey((Keys)Settings.Default["VolUpKey"], (int)Settings.Default["VolUpModKey"]);
			_volUp.Pressed += (sender, args) => SoundService.VolumeUp(this.Handle);
			_volUp.Register(this);

			_volDown = new Hotkey((Keys)Settings.Default["VolDownKey"], (int)Settings.Default["VolDownModKey"]);
			_volDown.Pressed += (sender, args) => SoundService.VolumeDown(this.Handle);
			_volDown.Register(this);

			_mute = new Hotkey((Keys)Settings.Default["MuteKey"], (int)Settings.Default["MuteModKey"]);
			_mute.Pressed += (sender, args) => SoundService.Mute(this.Handle);
			_mute.Register(this);
		}


		private void optionsToolStripMenuItem_Click(object sender, EventArgs e)
		{
			try
			{
				_volUp.Unregister();
				_volDown.Unregister();
				_mute.Unregister();
			}
			catch (NullReferenceException)
			{
			}

			var dialog = new OptionsDialog();
			dialog.Closed += (o, args) => LoadSettings();
			dialog.Show();
		}
	}
}
